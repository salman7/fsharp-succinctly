﻿module Program2

// define an organization type
type Organization = { chief: string; indians: string list }

// create an instance of this type
let wayneManor = 
    { chief = "Batman"; 
      indians = ["Robin"; "Alfred"] }

// access a field from this type
printfn "wayneManor.chief = %s" wayneManor.chief

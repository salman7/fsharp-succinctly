﻿module Program2

let result =
    match System.DateTime.Now.Second % 2 = 0 with
    | true -> "heads"
    | false ->  "tails"
        
printfn "%A" result


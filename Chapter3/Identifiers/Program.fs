﻿//Identifiers and Let Bindings
let x = 42

let myAdd = fun x y -> x + y

let raisePowerTwo x = x ** 2.0

let n = 10

let add a b = a + b

let result = add n 4

printfn "%i" (result)

System.Console.ReadLine()
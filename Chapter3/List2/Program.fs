﻿// create a list of one item
let one = ["one "]
// create a list of two items
let two = "two " :: one
// create a list of three items
let three = "three " :: two

// reverse the list of three items
let rightWayRound = List.rev three

printfn "%A" one
printfn "%A" two
printfn "%A" three
printfn "%A" rightWayRound
﻿module Program1

let booleanToString x =
    match x with false -> "False" | _ -> "True"
﻿open System
open System.Reflection
open System.Windows
open System.Windows.Markup
open System.Windows.Controls

// load a XAML file from the current assembly
let loadWindowFromResources name =
    let currentAssem = Assembly.GetExecutingAssembly()
    use xamlStream = currentAssem.GetManifestResourceStream(name)
    // resource stream will be null if not found
    if xamlStream = null then
        failwithf "The resouce stream '%s' was not found" name 
    XamlReader.Load(xamlStream) :?> Window 

      // dynamic lookup operator for controls
let (?) (c:obj) (s:string) =
    match c with 
    | :? FrameworkElement as c -> c.FindName(s) :?> 'T
    | _ -> failwith "dynamic lookup failed"


    // creates our main window and hooks up the events
let createMainWindow() =
    // load the window from the resource file
    let win = loadWindowFromResources "MainWindow.xaml"
    // find the relevant controls in the window
    let pressMeButton: Button = win?PressMeButton
    let messageTextBox: TextBox = win?MessageTextBox
    // wire up an event handler
    let onClick() = 
        MessageBox.Show(messageTextBox.Text) |> ignore
    pressMeButton.Click.Add(fun _ -> onClick())
    // return the newly created window
    win

// create the window
let win = createMainWindow()

// create the application object and show the window
let app = new Application()

[<STAThread>]
do app.Run win |> ignore
